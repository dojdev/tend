/**
 * The main javascript file
 */

$('.js-promo').slick({
	arrows: true,
	dots: true,
	vertical: true,
	useTransform: true,
	cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
});