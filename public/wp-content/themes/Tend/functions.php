<?php global $text_domain; $text_domain = "tend"; ?><?php
/**
 * i18n native support
 * Loading the 
 */

function load_i18n_files() {
  load_theme_textdomain("tend", get_template_directory() . '/languages/');
}

add_action('after_setup_theme', 'load_i18n_files');