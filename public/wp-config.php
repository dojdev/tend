<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'tend');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'vagrant');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<r8;<j9Q3l`Rat^$}32&qW mw~<=Ze6+ {6>mKNo[54eEq]|Lx6`vbM4o}JIRH@0');
define('SECURE_AUTH_KEY',  'KBg64Y>ypgoQq6aV2RpIKiW>igoFT.aSPfGIF2ml6FK8bQdI6<+.40;Rw._h}sH0');
define('LOGGED_IN_KEY',    'Cfa]~ vyj1TW>JgXd~Iny9Q1r&ot%S;VAp)s6,p3LnC{CNkTtL92&.5;Wy2}@(<g');
define('NONCE_KEY',        '{E~>EO3^q{ZCX{rBLK!$1pa-nZM)0`R+&GERO v/nx0;:[L/C8/4%,voZq8~:6]?');
define('AUTH_SALT',        'zU`0+1}iAaiN=a#w[~lSH$GLr:.14jZ0?6 |<}Ir}U#3b0/^F~t_GAVNokhh;kMM');
define('SECURE_AUTH_SALT', '^M)P83Cedq5aj?nO2ZB#dcp`*)5!6baxU3ma7p9xAa#&*nB1ZTWMBa-/d]cvs>h~');
define('LOGGED_IN_SALT',   'RgtLq$;G|Y%]I~.C60HY!m LE b=nKajFL0mrGAF2A(.o/^3]WkR_@@)T0;#V!2w');
define('NONCE_SALT',       ')MGlZDK/.Ng]F-y~x^XcB2A%I$c}beEV0^(-?J@-va}9{ss+q/}#OAL0&_6~RJth');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
